# Ansible Role: OpenLens

This role installs [OpenLens](https://github.com/MuhammedKalkan/OpenLens) binary on any supported host.

## Requirements

None

## Installing

The role can be installed by running the following command:

```bash
git clone https://gitlab.com/enmanuelmoreira/ansible-role-openlens enmanuelmoreira.openlens
```

Add the following line into your `ansible.cfg` file:

```bash
[defaults]
role_path = ../
```

## Role Variables

Available variables are listed below, along with default values (see `defaults/main.yml`):

    openlens_version: latest # tag "v6.0.2" if you want a specific version
    openlens_package_name: openlens
    openlens_repo_path: https://github.com/MuhammedKalkan/OpenLens/releases/download

This role always installs the desired version. See on [GitHub](https://github.com/MuhammedKalkan/OpenLens/releases/).

    openlens_version: latest # tag "v6.0.2" if you want a specific version

The version to the Lens application.

    openlens_package_name: lens

The name of the package installed in the system (if there need to be updated it).

    openlens_repo_path: https://github.com/MuhammedKalkan/OpenLens/releases/download

The complete path to the openlens repository.

## Role Specific Variables

Into each file on vars folder you can specify the architecture of openlens package depending of the OS flavor:

    openlens_arch: amd64 # debian: amd64, arm64 # fedora: x86_64, aarch64

## Dependencies

None.

## Example Playbook

    - hosts: all
      become: yes
      roles:
        - role: enmanuelmoreira.openlens

## License

MIT / BSD
